/*
 * Copyright (c) 2016, 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define LOG_TAG "RecorderIon"

#include "recorder/src/service/qmmf_recorder_ion.h"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <map>
#include <vector>

#include <linux/msm_ion.h>

#include "common/utils/qmmf_log.h"
#include "common/utils/qmmf_tools.h"
#include "recorder/src/service/qmmf_recorder_common.h"

namespace qmmf {
namespace recorder {

using ::std::map;
using ::std::queue;
using ::std::vector;

static const int kBufferAlign   = 4096;

RecorderIon::RecorderIon()
    : ion_device_(-1), buffer_size_(0), request_size_(0) {
  QMMF_DEBUG("%s() TRACE", __func__);
}

RecorderIon::~RecorderIon() {
  QMMF_DEBUG("%s() TRACE", __func__);

  if (!ion_buffer_map_.empty()) {
    int32_t result = Deallocate();
    assert(result == 0);
    QMMF_INFO("%s() deallocated all ion buffers", __func__);
  }
}

int32_t RecorderIon::Allocate(const int32_t number, const int32_t size) {
  QMMF_DEBUG("%s() TRACE", __func__);
  QMMF_VERBOSE("%s() INPARAM: number[%d]", __func__, number);
  QMMF_VERBOSE("%s() INPARAM: size[%d]", __func__, size);

  if (number <= 0) return -EINVAL;
  if (size <= 0) return -EINVAL;
  int result;
  request_size_ = size;
  buffer_size_ = (size + kBufferAlign - 1) & ~(kBufferAlign - 1);

  // open ion device
  ion_device_ = ion_open();
  if (ion_device_ < 0) {
    QMMF_ERROR("%s() error opening ion device: %d[%s]", __func__, errno,
               strerror(errno));
    return errno;
  }

  // Allocate Buffer
  for (int32_t index = 0; index < number; ++index) {
    RecorderIonBuffer buffer;
    uint32_t heap_id_mask = ION_HEAP(ION_SYSTEM_HEAP_ID);

    result = ion_alloc_fd(ion_device_, buffer_size_, 0, heap_id_mask, 0,
                          &buffer.map_fd);
    if (result) {
      QMMF_ERROR("%s() ion_alloc_fd  command failed: %d[%s]", __func__, errno,
                 strerror(errno));
      return errno;
    }

    ion_buffer_map_.insert({buffer.map_fd, buffer});
  }

  // map buffers into address space
  for (RecorderIonBufferMap::value_type& buffer_value : ion_buffer_map_) {
    buffer_value.second.data = mmap(NULL, buffer_size_, PROT_READ | PROT_WRITE,
                                    MAP_SHARED, buffer_value.second.map_fd, 0);
    if (buffer_value.second.data == MAP_FAILED) {
      QMMF_ERROR("%s() unable to map buffer[%d]: %d[%s]", __func__,
                 buffer_value.second.map_fd, errno, strerror(errno));

      result = close(buffer_value.second.map_fd);
      if (result < 0) {
        QMMF_ERROR("%s() error closing mapping fd[%d]: %d[%s]", __func__,
                   buffer_value.second.map_fd, errno, strerror(errno));
        QMMF_ERROR("%s() [CRITICAL] ion fd has leaked", __func__);
      }
      return result;
    }

    SyncStart(buffer_value.second.map_fd);

    QMMF_VERBOSE("%s() allocated ion buffer[%d][%s]", __func__,
                 buffer_value.first, buffer_value.second.ToString().c_str());
  }

  return 0;
}

int32_t RecorderIon::Deallocate() {
  QMMF_DEBUG("%s() TRACE", __func__);
  int result;

  if (ion_device_ == -1) {
    QMMF_WARN("%s() ion device is not opened", __func__);
    return 0;
  }

  for (RecorderIonBufferMap::value_type& buffer_value : ion_buffer_map_) {
    QMMF_VERBOSE("%s() deallocating ion buffer[%s]", __func__,
                 buffer_value.second.ToString().c_str());
    SyncEnd(buffer_value.second.map_fd);

    result = munmap(buffer_value.second.data, buffer_size_);
    if (result < 0)
      QMMF_ERROR("%s() unable to unmap buffer[%d]: %d[%s]", __func__,
                 buffer_value.second.map_fd, errno, strerror(errno));

    buffer_value.second.data = nullptr;

    result = close(buffer_value.second.map_fd);
    if (result < 0) {
      QMMF_ERROR("%s() error closing shared fd[%d]: %d[%s]", __func__,
                 buffer_value.second.map_fd, errno, strerror(errno));
      return errno;
    }

    buffer_value.second.map_fd = -1;
  }

  ion_buffer_map_.clear();
  QMMF_INFO("%s() deallocated all ion buffers", __func__);

  // close ion device
  result = ion_close(ion_device_);
  if (result < 0) {
    QMMF_ERROR("%s() error closing ion device[%d]: %d[%s]", __func__,
               ion_device_, errno, strerror(errno));
    return errno;
  }

  ion_device_ = -1;
  buffer_size_ = 0;
  request_size_ = 0;

  return 0;
}

int32_t RecorderIon::GetList(vector<BufferDescriptor>* buffers) {
  QMMF_DEBUG("%s() TRACE", __func__);

  if (ion_buffer_map_.empty()) {
    QMMF_WARN("%s() no ion buffers allocated", __func__);
    return 0;
  }

  for (RecorderIonBufferMap::value_type& buffer_value : ion_buffer_map_) {
    BufferDescriptor buffer = { buffer_value.second.data,
      buffer_value.second.map_fd,
      static_cast<uint32_t>(buffer_value.second.map_fd), 0U,
      static_cast<uint32_t>(request_size_), 0U, 0U, 0U };


    QMMF_VERBOSE("%s() OUTPARAM: buffer[%s]", __func__,
                 buffer.ToString().c_str());
    buffers->push_back(buffer);
  }

  return 0;
}

int32_t RecorderIon::GetList(queue<BufferDescriptor>* buffers) {
  QMMF_DEBUG("%s() TRACE", __func__);

  if (ion_buffer_map_.empty()) {
    QMMF_WARN("%s() no ion buffers allocated", __func__);
    return 0;
  }

  for (RecorderIonBufferMap::value_type& buffer_value : ion_buffer_map_) {
    BufferDescriptor buffer = { buffer_value.second.data,
      buffer_value.second.map_fd,
      static_cast<uint32_t>(buffer_value.second.map_fd), 0U,
      static_cast<uint32_t>(request_size_), 0U, 0U, 0U };

    QMMF_VERBOSE("%s() OUTPARAM: buffer[%s]", __func__,
                 buffer.ToString().c_str());
    buffers->push(buffer);
  }

  return 0;
}

int32_t RecorderIon::Import(const BnBuffer& bn_buffer,
                            BufferDescriptor* buffer) {
  QMMF_DEBUG("%s() TRACE", __func__);
  QMMF_VERBOSE("%s INPARAM: bn_buffer[%s]", __func__,
               bn_buffer.ToString().c_str());

  RecorderIonBufferMap::iterator ion_buffer_iterator =
      ion_buffer_map_.find(bn_buffer.buffer_id);
  if (ion_buffer_iterator == ion_buffer_map_.end()) {
    QMMF_ERROR("%s() no ion buffer for key[%d]", __func__,
               bn_buffer.buffer_id);
    return -EINVAL;
  }

  buffer->data = ion_buffer_iterator->second.data;
  buffer->fd = bn_buffer.buffer_id;
  buffer->capacity = request_size_;
  buffer->size = bn_buffer.size;
  buffer->timestamp = bn_buffer.timestamp;
  buffer->flag = bn_buffer.flag;

  QMMF_VERBOSE("%s() OUTPARAM: buffer[%s]", __func__,
               buffer->ToString().c_str());
  return 0;
}

int32_t RecorderIon::Export(const BufferDescriptor& buffer,
                            BnBuffer* bn_buffer) {
  QMMF_DEBUG("%s() TRACE", __func__);
  QMMF_VERBOSE("%s INPARAM: buffer[%s]", __func__,
               buffer.ToString().c_str());

  RecorderIonBufferMap::iterator ion_buffer_iterator =
      ion_buffer_map_.find(buffer.fd);
  if (ion_buffer_iterator == ion_buffer_map_.end()) {
    QMMF_ERROR("%s() no ion buffer for key[%d]", __func__, buffer.fd);
    return -EINVAL;
  }

  bn_buffer->ion_fd = buffer.fd;
  bn_buffer->ion_meta_fd = -1;
  bn_buffer->size = buffer.size;
  bn_buffer->timestamp = buffer.timestamp;
  bn_buffer->width = -1;
  bn_buffer->height = -1;
  bn_buffer->buffer_id = buffer.fd;
  bn_buffer->flag = buffer.flag;
  bn_buffer->capacity = buffer.capacity;

  QMMF_VERBOSE("%s() OUTPARAM: bn_buffer[%s]", __func__,
               bn_buffer->ToString().c_str());
  return 0;
}

}; // namespace recorder
}; // namespace qmmf
