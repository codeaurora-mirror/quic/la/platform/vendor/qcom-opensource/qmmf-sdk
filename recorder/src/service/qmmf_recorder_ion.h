/*
 * Copyright (c) 2016, 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <iomanip>
#include <map>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

#include <ion/ion.h>
#include <linux/dma-buf.h>
#include <linux/msm_ion.h>

#include "recorder/src/service/qmmf_recorder_common.h"

namespace qmmf {
namespace recorder {

class RecorderIon
{
 public:
  RecorderIon();
  ~RecorderIon();

  int32_t Allocate(const int32_t number, const int32_t size);
  int32_t Deallocate();

  int32_t GetList(::std::vector<BufferDescriptor>* buffers);
  int32_t GetList(::std::queue<BufferDescriptor>* buffers);

  int32_t Import(const BnBuffer& bn_buffer, BufferDescriptor* buffer);
  int32_t Export(const BufferDescriptor& buffer, BnBuffer* bn_buffer);

 private:
  struct RecorderIonBuffer {
    void* data;
    int32_t map_fd;
    ::std::string ToString() const {
      ::std::stringstream stream;
      stream << "data[" << data << "] ";
      stream << "map_fd[" << map_fd << "]] ";
      return stream.str();
    }
  };

  typedef ::std::map<int32_t, RecorderIonBuffer> RecorderIonBufferMap;

  RecorderIonBufferMap ion_buffer_map_;
  int32_t ion_device_;
  int32_t buffer_size_;
  int32_t request_size_;

  // disable copy, assignment, and move
  RecorderIon(const RecorderIon&) = delete;
  RecorderIon(RecorderIon&&) = delete;
  RecorderIon& operator=(const RecorderIon&) = delete;
  RecorderIon& operator=(const RecorderIon&&) = delete;
};

}; // namespace recorder
}; // namespace qmmf
