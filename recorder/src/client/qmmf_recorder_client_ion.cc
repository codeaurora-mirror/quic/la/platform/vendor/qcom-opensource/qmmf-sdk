/*
 * Copyright (c) 2016, 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define LOG_TAG "RecorderClientIon"

#include "recorder/src/client/qmmf_recorder_client_ion.h"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <map>
#include <linux/msm_ion.h>

#include "common/utils/qmmf_log.h"
#include "common/utils/qmmf_tools.h"
#include "include/qmmf-sdk/qmmf_recorder_params.h"
#include "recorder/src/client/qmmf_recorder_params_internal.h"
#include "recorder/src/client/qmmf_recorder_service_intf.h"

namespace qmmf {
namespace recorder {

using ::std::map;

RecorderClientIon::RecorderClientIon() : ion_device_(-1) {
  QMMF_DEBUG("%s() TRACE", __func__);

  // open ion device
  ion_device_ = ion_open();
  if (ion_device_ < 0)
    QMMF_ERROR("%s() error opening ion device: %d[%s]", __func__,
               errno, strerror(errno));
}

RecorderClientIon::~RecorderClientIon() {
  QMMF_DEBUG("%s() TRACE", __func__);
  int32_t result;

  if (ion_device_ == -1)
    QMMF_WARN("%s() ion device is not opened", __func__);

  // release all ion buffers
  for (auto& client_map : buffer_map_) {
    result = Release(client_map.first);
    if (result < 0) {
      QMMF_ERROR("%s() unable to release buffers for client[%d]: %d",
          __func__, client_map.first, result);
    }
  }
  // close ion device
  result = ion_close(ion_device_);
  if (result < 0) {
    QMMF_ERROR("%s() error closing ion device[%d]: %d[%s]", __func__,
        ion_device_, errno, strerror(errno));
  }
}

int32_t RecorderClientIon::Associate(uint32_t track_id,
                                     const BnBuffer& bn_buffer,
                                     BufferDescriptor* buffer) {
  QMMF_DEBUG("%s() TRACE", __func__);
  QMMF_VERBOSE("%s() INPARAM: track_id[%d]", __func__, track_id);
  QMMF_VERBOSE("%s() INPARAM: bn_buffer[%s]", __func__,
               bn_buffer.ToString().c_str());
  int result;
  std::map<uint32_t, RecorderClientIonBufferMap>::iterator client_map;

  if (ion_device_ == -1) {
    QMMF_ERROR("%s() ion device is not opened", __func__);
    return -ENODEV;
  }

  {
    std::lock_guard<std::mutex> l(buffer_map_mutex_);
    client_map = buffer_map_.find(track_id);
    if (client_map != buffer_map_.end()) {
      auto ion_buffer = client_map->second.find(bn_buffer.buffer_id);
      if (ion_buffer != client_map->second.end()) {
        // found the ion buffer
        result = close(bn_buffer.ion_fd);
        if (result < 0) {
          QMMF_ERROR("%s() error closing ion_fd[%d]: %d[%s]", __func__,
                     bn_buffer.ion_fd, errno, strerror(errno));
          QMMF_ERROR("%s() [CRITICAL] ion fd has leaked", __func__);
        }

        buffer->fd = ion_buffer->second.map_fd;
        buffer->data = ion_buffer->second.data;
        buffer->size = bn_buffer.size;
        buffer->offset = 0;
        buffer->timestamp = bn_buffer.timestamp;
        buffer->flag = bn_buffer.flag;
        buffer->buf_id = bn_buffer.buffer_id;
        buffer->capacity = bn_buffer.capacity;

        QMMF_VERBOSE("%s() OUTPARAM: buffer[%s]", __func__,
                     buffer->ToString().c_str());
        return 0;
      }
    } else {
      // create new client map
      buffer_map_.insert({track_id, RecorderClientIonBufferMap()});
      client_map = buffer_map_.find(track_id);
    }
  }


  RecorderClientIonBuffer ion_buffer;
  ion_buffer.capacity = bn_buffer.capacity;
  ion_buffer.map_fd = bn_buffer.ion_fd;

  ion_buffer.data = mmap(NULL, ion_buffer.capacity, PROT_READ | PROT_WRITE,
                         MAP_SHARED, ion_buffer.map_fd, 0);
  if (ion_buffer.data == MAP_FAILED) {
    QMMF_ERROR("%s() unable to map buffer[%d]: %d[%s]", __func__,
               ion_buffer.map_fd, errno, strerror(errno));

    result = close(ion_buffer.map_fd);
    if (result < 0) {
      QMMF_ERROR("%s() error closing mapping fd[%d]: %d[%s]", __func__,
                 ion_buffer.map_fd, errno, strerror(errno));
      QMMF_ERROR("%s() [CRITICAL] ion fd has leaked", __func__);
    }

    return errno;
  }

  SyncStart(ion_buffer.map_fd);

  buffer->data = ion_buffer.data;
  buffer->size = bn_buffer.size;
  buffer->timestamp = bn_buffer.timestamp;
  buffer->flag = bn_buffer.flag;
  buffer->buf_id = bn_buffer.buffer_id;
  buffer->capacity = bn_buffer.capacity;
  buffer->fd = bn_buffer.ion_fd;

  QMMF_VERBOSE("%s() mapped ion buffer[%s]", __func__,
               ion_buffer.ToString().c_str());

  std::lock_guard<std::mutex> l(buffer_map_mutex_);
  // save ion buffer
  client_map->second.insert({bn_buffer.buffer_id, ion_buffer});

  QMMF_VERBOSE("%s() OUTPARAM: buffer[%s]", __func__,
               buffer->ToString().c_str());
  return 0;
}

int32_t RecorderClientIon::Release(uint32_t track_id) {
  QMMF_DEBUG("%s() TRACE", __func__);
  QMMF_VERBOSE("%s() INPARAM: track_id[%u]", __func__, track_id);

  std::map<uint32_t, RecorderClientIonBufferMap>::iterator client_map;

  {
    std::lock_guard<std::mutex> l(buffer_map_mutex_);
    client_map = buffer_map_.find(track_id);
    if (client_map == buffer_map_.end()) {
      QMMF_INFO("%s() no ion buffers for track_id", __func__);
      return 0;
    }
  }

  for (auto& buffer : client_map->second) {
    int result;

    QMMF_VERBOSE("%s() releasing ion buffer[%s]", __func__,
                 buffer.second.ToString().c_str());
    if (buffer.second.map_fd)
      SyncEnd(buffer.second.map_fd);

    result = munmap(buffer.second.data, buffer.second.capacity);
    if (result < 0) {
      QMMF_ERROR("%s() unable to unmap buffer[%d]: %d[%s]", __func__,
                 buffer.second.map_fd, errno, strerror(errno));
      close(buffer.second.map_fd);
      return errno;
    }

    buffer.second.data = nullptr;

    result = close(buffer.second.map_fd);
    if (result < 0) {
      QMMF_ERROR("%s() error closing shared fd[%d]: %d[%s]", __func__,
                 buffer.second.map_fd, errno, strerror(errno));
      return errno;
    }
    buffer.second.map_fd = -1;
  }

  std::lock_guard<std::mutex> l(buffer_map_mutex_);
  client_map->second.clear();
  buffer_map_.erase(client_map->first);
  QMMF_INFO("%s() released all ion buffers", __func__);

  return 0;
}

}; // namespace recorder
}; // namespace qmmf
