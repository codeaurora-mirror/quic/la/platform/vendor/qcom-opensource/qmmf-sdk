LOCAL_PATH := $(call my-dir)

QMMF_SDK_TOP_SRCDIR := $(LOCAL_PATH)/../..

include $(QMMF_SDK_TOP_SRCDIR)/build.mk

ifneq (,$(BUILD_QMMMF))

# Build qmmf camera hal reprocess library
# libqmmf_common_jpeg_encoder.so

include $(CLEAR_VARS)

include $(QMMF_SDK_TOP_SRCDIR)/common.mk

LOCAL_C_INCLUDES += $(CAMERA_HAL_PATH)/QCamera2/HAL3
LOCAL_C_INCLUDES += $(TARGET_OUT_HEADERS)/mm-core/omxcore
LOCAL_C_INCLUDES += $(MEDIA_HAL_PATH)
LOCAL_C_INCLUDES += $(CAMERA_HAL_PATH)/QCamera2/stack/common
LOCAL_C_INCLUDES += $(CAMERA_HAL_PATH)/mm-image-codec/qexif
LOCAL_C_INCLUDES += $(CAMERA_HAL_PATH)/mm-image-codec/qomx_core
ifneq ($(DISABLE_PP_JPEG),1)
LOCAL_SRC_FILES := qmmf_jpeg_encoder.cc
endif

LOCAL_SHARED_LIBRARIES += libcamera_client libqmmf_utils
ifneq ($(DISABLE_PP_JPEG),1)
LOCAL_SHARED_LIBRARIES += libmmjpeg_interface
endif

LOCAL_MODULE = libqmmf_common_jpeg_encoder

include $(BUILD_SHARED_LIBRARY)

endif # BUILD_QMMMF
