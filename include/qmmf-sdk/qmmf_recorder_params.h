/*
* Copyright (c) 2016-2021, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! @file qmmf_recorder_params.h
*/

#pragma once

#include <sys/types.h>

#include <cstddef>
#include <memory>
#include <iomanip>
#include <functional>
#include <sstream>
#include <string>
#include <cstdint>
#include <type_traits>
#include <vector>

#include <camera/CameraMetadata.h>

#include "qmmf-sdk/qmmf_buffer.h"
#include "qmmf-sdk/qmmf_codec.h"
#include "qmmf-sdk/qmmf_device.h"

namespace qmmf {

namespace recorder {

#define MAX_IN_DEVICES 4

#define MAX_AUDIO_INPUT_DEVICES (10)
#define MAX_AUDIO_PROFILE (80)
#define MAX_THUMBNAIL_IMAGE_PARAM (2)

typedef int32_t status_t;

enum class EventType {
  kServerDied    = 1,
  kCameraError   = 2,
  kCameraOpened  = 3,
  kCameraClosing = 4,
  kCameraClosed  = 5,
};

typedef std::function<void(EventType event_type, void *event_data,
                           size_t event_data_size)> EventCb;

/// @brief Recorder callback is called to notify non track
/// and non session specific event notifications
///
/// Only error event types are expected as of now
struct RecorderCb {
  EventCb event_cb;
};

/// @brief RecorderErrorData is used to determine the type of recorer errors.
struct RecorderErrorData {
  uint32_t        camera_id;
  int32_t         error_code;
};

/// @brief Session cb is used to return state changes i.e. to indicate
/// start, stop, pause state transition completions
struct SessionCb {
  EventCb event_cb;
};

/// @brief MetaParamType flag is used to determine type of meta data set in
/// MetaData structure.
enum class MetaParamType {
  kNone               = (1 << 0),
  kCamBufMetaData     = (1 << 1),
  kVideoFrameType     = (1 << 2),
  kCamMetaFrameNumber = (1 << 3)
};

/// @brief This struct is used to report different types of meta data associated
/// with BufferDescriptor.
struct MetaData {
  uint32_t meta_flag;
  CameraBufferMetaData cam_buffer_meta_data;
  uint32_t video_frame_type_info;
  uint32_t cam_meta_frame_number;
};

/// @brief Plugin related information exposed to the client
struct PluginInfo {
  /// plugin name
  std::string name;
  /// version of the underlying library
  std::string version;
  /// indicating whether runtime enable/disable is supported
  bool        togglable;
  /// tuning file name
  std::string tuning_file_name;

  PluginInfo()
    : name(), version("0.0"), togglable(false), tuning_file_name() {}

  PluginInfo(const std::string &name,
             const std::string &version,
             const bool togglable)
      : name(name),
        version(version),
        togglable(togglable) {
    // set default tuning file name
    tuning_file_name = kTuningPrefix + name + kTuningSuffix;
  }

  PluginInfo(const void *blob, const size_t size) { FromBlob(blob, size); }

  void SetTuningFile(const std::string &tuning) {
    tuning_file_name = tuning;
  }

  std::string ToString(uint32_t indent = 0) const {
    std::stringstream indentation;
    for (uint32_t i = 0; i < indent; i++) indentation << '\t';
    indent++;

    std::stringstream stream;
    stream << indentation.str()
           << "\"name\" : " << name << '\n';
    stream << indentation.str()
           << "\"version\" : " << version << '\n';
    stream << indentation.str()
           << "\"togglable\" : " << togglable << '\n';
    stream << indentation.str()
           << "\"tuning file\" : " << tuning_file_name << '\n';
    return stream.str();
  }

  std::shared_ptr<void> ToBlob() const {
    std::shared_ptr<void> blob(new uint8_t[Size()],
                               std::default_delete<uint8_t[]>() );

    uint16_t name_size = (uint16_t)name.size();
    uint16_t version_size = (uint16_t)version.size();
    uint16_t tuning_size = (uint16_t)tuning_file_name.size();

    uintptr_t dest = reinterpret_cast<uintptr_t>(blob.get());
    memcpy(reinterpret_cast<void *>(dest),
           reinterpret_cast<void *>(&name_size), sizeof(uint16_t));

    dest += sizeof(uint16_t);
    memcpy(reinterpret_cast<void *>(dest),
           reinterpret_cast<void *>(&version_size), sizeof(uint16_t));

    dest += sizeof(uint16_t);
    memcpy(reinterpret_cast<void *>(dest),
           reinterpret_cast<void *>(&tuning_size), sizeof(uint16_t));

    dest += sizeof(uint16_t);
    memcpy(reinterpret_cast<void *>(dest), name.data(), name.size());

    dest += name.size();
    memcpy(reinterpret_cast<void *>(dest), version.data(), version.size());

    dest += version.size();
    memcpy(reinterpret_cast<void *>(dest), &togglable, sizeof(togglable));

    dest += sizeof(togglable);
    memcpy(reinterpret_cast<void *>(dest), tuning_file_name.data(),
           tuning_file_name.size());

    return blob;
  }

  void FromBlob(const void *blob, const size_t size) {
    uintptr_t src = reinterpret_cast<uintptr_t>(blob);
    uint16_t name_size, version_size, tuning_size;
    size_t remaining_size = size;
    name = "Invalid blob";
    version = "Invalid blob";
    tuning_file_name = "Invalid blob";

    if (remaining_size > sizeof(uint16_t) * 3) {
      remaining_size -= sizeof(uint16_t) * 3;
      memcpy(&name_size, reinterpret_cast<void *>(src), sizeof(uint16_t));

      src += sizeof(uint16_t);
      memcpy(&version_size, reinterpret_cast<void *>(src), sizeof(uint16_t));

      src += sizeof(uint16_t);
      memcpy(&tuning_size, reinterpret_cast<void *>(src), sizeof(uint16_t));

      if (remaining_size >= version_size + name_size + tuning_size) {
        src += sizeof(uint16_t);
        name.assign(reinterpret_cast<const char *>(src), name_size);

        src += name_size;
        version.assign(reinterpret_cast<const char *>(src), version_size);

        src += version_size;
        memcpy(&togglable, reinterpret_cast<void *>(src), sizeof(togglable));

        src += sizeof(togglable);
        tuning_file_name.assign(reinterpret_cast<const char *>(src),
            tuning_size);
      }
    }
  }

  size_t Size() const {
    return (sizeof(uint16_t) + name.size() +
            sizeof(uint16_t) + version.size() +
            sizeof(togglable) +
            sizeof(uint16_t) + tuning_file_name.size());
  }

private:

  /// recommended tuning file prefix
  const std::string kTuningPrefix = "qmmf_tuning_";
  /// recommended tuning file suffix
  const std::string kTuningSuffix = ".bin";
};

typedef std::vector<PluginInfo> SupportedPlugins;

/// @brief Both data and event callbacks should be set by the client.
/// event_cb is called to notify track specific errors and data_cb
/// to notify availability of output data from track to clients
///
/// TrackMetaParam in data cb is an optional parameter. This parameter is
/// expected
/// to be used in case multiple frames are passed in the same buffer and
/// in that case meta_param can describe the respective frame offsets
/// and timestamps in the buffer
/// When the data cb is called by recorder, the buffer ownership is transfered
/// to
/// client. To return the buffer back to recoder, clients should call
/// ReturnTrackBuffer
/// API. However, clients needs to ensure that buffers returned within the frame
/// rate
/// of track - else recording pipeline will stall.
/// Track event_cb returns async error events and data_cb returns periodic
/// data
/// Note that both callback implementations need to be re-entrant.
struct TrackCb {
  std::function<void(uint32_t track_id, ::std::vector<BufferDescriptor> buffers,
                     ::std::vector<MetaData> meta_data)> data_cb;
  std::function<void(uint32_t track_id, EventType event_type, void *event_data,
                     size_t event_data_size)> event_cb;
};

/// @brief Createtime parameters for audio track
///
/// Audio output device is used for routing audio to output
/// to external devices say through HDMI. In all other usecases
/// out_device will be set to AUDIO_DEVICE_NONE
struct AudioTrackCreateParam {
  uint32_t                in_devices_num;
  DeviceId                in_devices[MAX_AUDIO_INPUT_DEVICES];
  uint32_t                sample_rate;
  uint32_t                channels;
  uint32_t                bit_depth;
  char                    profile[MAX_AUDIO_PROFILE];
  AudioFormat             format;
  AudioCodecParams        codec_params;
  DeviceId                out_device;
  uint32_t                flags;

  AudioTrackCreateParam() {
    memset(profile, 0x0, sizeof(profile));
  }

  ::std::string ToString() const {
    ::std::stringstream stream;
    stream << "in_devices[";
    for (uint32_t i = 0; i < in_devices_num; i++)
      stream << in_devices[i] << ", ";
    stream << "SIZE[" << in_devices_num << "]], ";
    stream << "sample_rate[" << sample_rate << "] ";
    stream << "channels[" << channels << "] ";
    stream << "bit_depth[" << bit_depth << "] ";
    stream << "profile[" << ::std::string(profile) << "] ";
    stream << "format["
           << static_cast<::std::underlying_type<AudioFormat>::type>(format)
           << "] ";
    stream << "codec_params[" << codec_params.ToString(format) << "] ";
    stream << "out_device[" << out_device << "] ";
    stream << "flags[" << flags << "]";
    return stream.str();
  }
};

/// @brief create time parameters for a video track
/// For 360 degree capture, camera_id vector should contain the id of
/// multiple cameras involved in 360 capture
/// \TODO: define VideoOutDevice
struct VideoTrackCreateParam {
  /// Video Track camera id
  uint32_t camera_id;
  /// Video Track width
  uint32_t width;
  /// Video Track height
  uint32_t height;
  /// Video Track frame rate
  float frame_rate;
  VideoFormat format_type;
  VideoCodecParams codec_param;
  bool do_vqzip;
  VQZipInfo vqzip_params;
  /// Extra buffer count
  uint32_t extra_buffer_count;

  VideoTrackCreateParam(uint32_t cam_id = 0,
                        VideoFormat fmt = VideoFormat::kNV12, uint32_t w = 3840,
                        uint32_t h = 2160, float frm_rate = 30,
                        uint32_t extra_buff_count = 0) {
    camera_id = cam_id;
    width = w;
    height = h;
    frame_rate = frm_rate;
    format_type = fmt;
    extra_buffer_count = extra_buff_count;
    switch (format_type) {
      case VideoFormat::kAVC:
        setAVCDefaultVideoParam();
        break;
      case VideoFormat::kHEVC:
        setHEVCDefaultVideoParam();
        break;
      case VideoFormat::kJPEG:
        setJPEGDefaultParam();
        break;
      default: {
        // Nothing to do for other formats
        codec_param = {};
      }
    }

    // Setting VQZipInfo parameters
    do_vqzip = false;
    vqzip_params = {};
  }

  ::std::string ToString() const {
    ::std::stringstream stream;
    stream << "camera_id[" << camera_id << "] ";
    stream << "width[" << width << "] ";
    stream << "height[" << height << "] ";
    stream << "frame_rate[" << frame_rate << "] ";
    stream << "format_type["
           << static_cast<::std::underlying_type<VideoFormat>::type>(
                  format_type)
           << "] ";
    stream << "extra_buffer_count[" << extra_buffer_count << "] ";
    stream << "codec_params[" << codec_param.ToString(format_type) << "] ";
    stream << "do_vqzip[" << ::std::boolalpha << do_vqzip << ::std::noboolalpha
           << "] ";
    stream << "vqzip_params[" << vqzip_params.ToString() << "]";
    return stream.str();
  }

  void setAVCDefaultVideoParam() {
    // Setting default Parameters for AVC
    codec_param.avc.idr_interval = 1;
    codec_param.avc.bitrate = 6000000;
    codec_param.avc.profile = AVCProfileType::kHigh;
    codec_param.avc.level = AVCLevelType::kLevel5_1;
    codec_param.avc.ratecontrol_type = VideoRateControlType::kMaxBitrate;
    codec_param.avc.qp_params.enable_init_qp = true;
    codec_param.avc.qp_params.init_qp.init_IQP = 27;
    codec_param.avc.qp_params.init_qp.init_PQP = 28;
    codec_param.avc.qp_params.init_qp.init_BQP = 28;
    codec_param.avc.qp_params.init_qp.init_QP_mode = 0x7;
    codec_param.avc.qp_params.enable_qp_range = true;
    codec_param.avc.qp_params.qp_range.min_QP = 10;
    codec_param.avc.qp_params.qp_range.max_QP = 51;
    codec_param.avc.qp_params.enable_qp_IBP_range = true;
    codec_param.avc.qp_params.qp_IBP_range.min_IQP = 10;
    codec_param.avc.qp_params.qp_IBP_range.max_IQP = 51;
    codec_param.avc.qp_params.qp_IBP_range.min_PQP = 10;
    codec_param.avc.qp_params.qp_IBP_range.max_PQP = 51;
    codec_param.avc.qp_params.qp_IBP_range.min_BQP = 10;
    codec_param.avc.qp_params.qp_IBP_range.max_BQP = 51;
    codec_param.avc.ltr_count = 0;
    codec_param.avc.insert_aud_delimiter = true;
    codec_param.avc.hier_layer = 0;
    codec_param.avc.prepend_sps_pps_to_idr = false;
    codec_param.avc.sar_enabled = false;
    codec_param.avc.sar_width = 0;
    codec_param.avc.sar_height = 0;
    codec_param.avc.slice_enabled = false;
    codec_param.avc.slice_header_spacing = 1024;
  }

  void setAVCVariableFramerateVideoParam() {
    // Setting Variable Framerate Video Parameters for AVC
    codec_param.avc.idr_interval = 1;
    codec_param.avc.bitrate      = 10000000;
    codec_param.avc.profile = AVCProfileType::kBaseline;
    codec_param.avc.level   = AVCLevelType::kLevel3;
    codec_param.avc.ratecontrol_type =
        VideoRateControlType::kVariableSkipFrames;
    codec_param.avc.qp_params.enable_init_qp = true;
    codec_param.avc.qp_params.init_qp.init_IQP = 51;
    codec_param.avc.qp_params.init_qp.init_PQP = 51;
    codec_param.avc.qp_params.init_qp.init_BQP = 51;
    codec_param.avc.qp_params.init_qp.init_QP_mode = 0x7;
    codec_param.avc.qp_params.enable_qp_range = true;
    codec_param.avc.qp_params.qp_range.min_QP = 26;
    codec_param.avc.qp_params.qp_range.max_QP = 51;
    codec_param.avc.qp_params.enable_qp_IBP_range = true;
    codec_param.avc.qp_params.qp_IBP_range.min_IQP = 26;
    codec_param.avc.qp_params.qp_IBP_range.max_IQP = 51;
    codec_param.avc.qp_params.qp_IBP_range.min_PQP = 26;
    codec_param.avc.qp_params.qp_IBP_range.max_PQP = 51;
    codec_param.avc.qp_params.qp_IBP_range.min_BQP = 26;
    codec_param.avc.qp_params.qp_IBP_range.max_BQP = 51;
    codec_param.avc.insert_aud_delimiter = true;
  }

  void setHEVCDefaultVideoParam() {
    // Setting default Parameters for HEVC
    codec_param.hevc.idr_interval = 1;
    codec_param.hevc.bitrate = 6000000;
    codec_param.hevc.profile = HEVCProfileType::kMain;
    codec_param.hevc.level = HEVCLevelType::kLevel5_1;
    codec_param.hevc.ratecontrol_type = VideoRateControlType::kMaxBitrate;
    codec_param.hevc.qp_params.enable_init_qp = true;
    codec_param.hevc.qp_params.init_qp.init_IQP = 27;
    codec_param.hevc.qp_params.init_qp.init_PQP = 28;
    codec_param.hevc.qp_params.init_qp.init_BQP = 28;
    codec_param.hevc.qp_params.init_qp.init_QP_mode = 0x7;
    codec_param.hevc.qp_params.enable_qp_range = true;
    codec_param.hevc.qp_params.qp_range.min_QP = 10;
    codec_param.hevc.qp_params.qp_range.max_QP = 51;
    codec_param.hevc.qp_params.enable_qp_IBP_range = true;
    codec_param.hevc.qp_params.qp_IBP_range.min_IQP = 10;
    codec_param.hevc.qp_params.qp_IBP_range.max_IQP = 51;
    codec_param.hevc.qp_params.qp_IBP_range.min_PQP = 10;
    codec_param.hevc.qp_params.qp_IBP_range.max_PQP = 51;
    codec_param.hevc.qp_params.qp_IBP_range.min_BQP = 10;
    codec_param.hevc.qp_params.qp_IBP_range.max_BQP = 51;
    codec_param.hevc.ltr_count = 0;
    codec_param.hevc.insert_aud_delimiter = true;
    codec_param.hevc.hier_layer = 0;
    codec_param.hevc.prepend_sps_pps_to_idr = false;
    codec_param.hevc.sar_enabled = false;
    codec_param.hevc.sar_width = 0;
    codec_param.hevc.sar_height = 0;
  }

  void setJPEGDefaultParam() {

    codec_param.jpeg.enable_thumbnail = false;
    codec_param.jpeg.quality = 95;
    codec_param.jpeg.thumbnail_quality = 75;
    codec_param.jpeg.thumbnail_height = 240;
    codec_param.jpeg.thumbnail_width = 320;
  }

};

/// @brief Result callback passed to StartCamera API
///
/// Optional result callback which will get triggered
/// by service once there is at least one started session
/// which includes a video track.
typedef std::function<void(uint32_t camera_id,
                           const android::CameraMetadata &res)> CameraResultCb;

/// @brief For thumbnail images only kJPEG is supported
/// For YUV and Bayer formats, quality is ignored
struct ImageParam {
  /// Image width
  uint32_t    width;
  /// Image height
  uint32_t    height;
  /// Image image quality (ignored for YUV and Bayer formats)
  uint32_t    image_quality;
  /// Image format
  ImageFormat image_format;

  ImageParam(): width(0), height(0), image_quality(95),
      image_format(ImageFormat::kJPEG) {}

  ::std::string ToString() const {
    ::std::stringstream stream;
    stream << "width[" << width << "]";
    stream << "height[" << height << "] ";
    stream << "image_quality[" << image_quality << "] ";
    stream << "image_format["
           << static_cast<::std::underlying_type<ImageFormat>::type>
                         (image_format)
           << "]";
    return stream.str();
  }
};

/// \brief ZSL queue parameters
///
/// Images in ZSL queue might have different dimension than final image.
struct ZslQueueParam {
  uint32_t    width;
  uint32_t    height;
  uint32_t    queue_depth;
  ImageFormat image_format;

  ZslQueueParam()
    : width(3840),
      height(2160),
      queue_depth(4),
      image_format(ImageFormat::kNV21) {}

  ::std::string ToString() const {
    ::std::stringstream stream;
    stream << "width[" << width << "]";
    stream << "height[" << height << "] ";
    stream << "queue_depth[" << queue_depth << "] ";
    stream << "image_format["
           << static_cast<::std::underlying_type<ImageFormat>::type>
                         (image_format)
           << "]";
    return stream.str();
  }
};

typedef std::function<void(uint32_t camera_id, uint32_t image_sequence_count,
                           BufferDescriptor buffer, MetaData meta_data)>
    ImageCaptureCb;

};
};  // namespace qmmf::recorder
